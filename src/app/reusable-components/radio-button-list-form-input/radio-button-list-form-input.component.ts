import {Component, Input} from '@angular/core';
import {FormBaseHelperComponent} from '..//form-base.component';

@Component({
  selector: 'app-radio-button-list-form-input',
  templateUrl: './radio-button-list-form-input.component.html',
  styleUrls: ['./radio-button-list-form-input.component.scss']
})

export class RadioButtonListFormInputComponent extends FormBaseHelperComponent {

  @Input() buttonList: [];

  isRadioButtonChecked(buttonValue: string): boolean {
    if ( this.value === null || this.value === undefined) {
      return false;
    }
    return buttonValue === this.value.toString();
  }
}
