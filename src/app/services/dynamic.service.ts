import {ComponentFactoryResolver, Injectable} from '@angular/core';
import {EMPTY, from, Observable, of, Subject, BehaviorSubject} from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  mergeMap,
  publish,
  tap,
  switchMap,
} from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { select } from '@angular-redux/store';

@Injectable({
  providedIn: 'root'
})
export class DynamicService {
    @select(['data'])
    private data$;
    @select(['data', 'referentiel'])
    referentiel$;
    public buildComponent$;
    public fieldObserver;
    public fieldDefinitions: any[];
    constructor(
        private componentFactoryResolver: ComponentFactoryResolver,
        private http: HttpClient
    ) {
        this.buildComponent$ = new Subject();
    }

    public getFormulaireData$(): Observable<any> {
        return this.data$;
    }

    public getReferentielFormulaire$(): Observable<any> {
        return this.referentiel$;
    }


    public initServiceFactory(transputList) {
        this.fieldDefinitions = this.setFieldsDefinition(transputList);
    }

    public setFieldsDefinition(transputList: any[]) {
        return transputList.map((fieldDefinition) => ({
            ...fieldDefinition,
            prop$: this.createFieldPropsObservable(fieldDefinition),
        }));
    }


    public createFieldsObservable(formContainer) {
        return this.buildComponent$
            .pipe(
                tap(() => formContainer.clear()),
                mergeMap(() => this.fieldDefinitions),
                map(({componentType, ...fieldDefinition}) => ({
                    ...fieldDefinition,
                    component: this.createComponent(componentType, formContainer, fieldDefinition.customClass),
                })),
                publish()
            );
    }


    public createFieldsPropsObservable(fields$) {
        return fields$
            .pipe(mergeMap(
                ({prop$}) => prop$,
                ({component}, {propname, value}) => ({component, propname, value}),
            ));
    }

    public createFieldsOutputsObservable(fields$) {
        return fields$
            .pipe(mergeMap(
                () => from(['myInput', 'change', 'submit']),
                ({id, component}, event) => ({id, component, event})
            ),
            mergeMap(
                ({component, event}) => component[event] || EMPTY,
                ({id, event}, value) => ({
                    event,
                    fieldId: id,
                    value,
                }),
            ));
    }

    private createFieldPropContentObservable(definition) {
        if (typeof definition !== 'function') {
            return of(definition);
        }

        return this.getReferentielFormulaire$()
            .pipe(
                switchMap((referentiel) =>
                        this.getFormulaireData$().pipe(
                            map((formData) => Object.assign({}, referentiel, formData)),
                        ),
                ),
                map(definition),
                distinctUntilChanged()
            );
    }

    private createFieldPropsObservable({id, data}) {
        const selectFieldValue = (formData) => formData &&
            formData.values &&
            formData.values[id];

        return from(Object.entries({
            id,
            ...data,
            value: selectFieldValue,
        }))
            .pipe(
                mergeMap(
                    ([propname, definition]) =>
                        this.createFieldPropContentObservable(definition).pipe(map(value => ({propname, value}))),
                )
            );
    }

    protected createComponent(ComponentClass, container, customClass?) {
        const componentFactory = this.componentFactoryResolver.resolveComponentFactory(ComponentClass);
        const component = container.createComponent(componentFactory);
        this.addClassToComponent(component, customClass);
        return component.instance;
    }

    private addClassToComponent(component, customClass) {
        if (customClass) {
            component.location.nativeElement.className = customClass;
        } else {
            component.location.nativeElement.className = 'col-12';
        }
    }


    protected getFieldDefinitionsSelection(sidebarItemId) {
        return from(this.fieldDefinitions).pipe(
            filter((fieldDefinition) => fieldDefinition.sidebarItem === sidebarItemId)
        );
    }
}
