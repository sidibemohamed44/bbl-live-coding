export const getReferentielProperty = (expertiseData: any, id: string, property?: string) => {
    try {
        const value = property ? expertiseData.FIELD_DEFINITIONS[id][property] :
          expertiseData.FIELD_DEFINITIONS[id];
        return value;
    } catch {
        return;
    }
  };
