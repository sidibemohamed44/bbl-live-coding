import {Component, ElementRef, EventEmitter, Input, Output} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Component({
  template: '',
})
export class FormBaseHelperComponent {
  @Input() public id: string;
  @Input() public label: string;
  @Input() public placeholder: string;
  @Input() public value: string;

  @Output() public myInput = new EventEmitter();

  constructor() {}

  public handleInputChanges(value): void {
    this.myInput.emit(value);
  }

}
