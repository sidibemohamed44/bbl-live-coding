import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TextFormInputComponent } from './text-form-input.component';

describe('SelectBoxInputComponent', () => {
  let component: TextFormInputComponent;
  let fixture: ComponentFixture<TextFormInputComponent>;

  const expectedHTML =
'\n  <label>\n'
+ '    labelTest :\n'
+ '  </label>\n'
+ '  <div>\n'
+ '    <input type="text" placeholder="">\n'
+ '  </div>\n';

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TextFormInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TextFormInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
