import {Component, Input} from '@angular/core';
import {FormBaseHelperComponent} from '../form-base.component';

@Component({
  selector: 'app-text-form-input',
  templateUrl: './text-form-input.component.html',
  styleUrls: ['./text-form-input.component.scss']
})
export class TextFormInputComponent extends FormBaseHelperComponent {
  @Input() public limit: string;
}
