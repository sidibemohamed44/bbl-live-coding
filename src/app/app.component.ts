import { Component, ViewChild, ViewContainerRef, OnInit, OnDestroy, ChangeDetectorRef, ViewEncapsulation } from '@angular/core';
import { DynamicService } from './services/dynamic.service';
import transputList from './datas/transput-list';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { NgRedux } from '@angular-redux/store';
import { IAppState, createActionUpdateFieldValue } from 'src/store/actions';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy {
  @ViewChild('formContainer', {read: ViewContainerRef}) formContainer: ViewContainerRef;
  private unsubscriber$ = new Subject();
  constructor(
    private dynamicService: DynamicService,
    private ngRedux: NgRedux<IAppState>,
    private changeDetector: ChangeDetectorRef) {}

  ngOnInit() {
    this.dynamicService.initServiceFactory(transputList);

    const fields$ = this.dynamicService.createFieldsObservable(this.formContainer);

    this.dynamicService.createFieldsPropsObservable(fields$)
      .pipe(takeUntil(this.unsubscriber$))
      .subscribe(({component, propname, value}) => {
        component[propname] = value;
        this.changeDetector.detectChanges();
    });

    this.dynamicService.createFieldsOutputsObservable(fields$)
      .pipe(takeUntil(this.unsubscriber$))
      .subscribe((event) => this.ngRedux.dispatch(createActionUpdateFieldValue(event.fieldId, event.value)));

    fields$.connect();
    this.dynamicService.buildComponent$.next();
  }

  ngOnDestroy() {
    this.unsubscriber$.next();
  }
}
