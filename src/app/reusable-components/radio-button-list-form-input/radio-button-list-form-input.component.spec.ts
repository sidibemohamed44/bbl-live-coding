import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RadioButtonListFormInputComponent} from './radio-button-list-form-input.component';

describe('RadioButtonListFormInputComponent', () => {
  let component: RadioButtonListFormInputComponent;
  let fixture: ComponentFixture<RadioButtonListFormInputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadioButtonListFormInputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadioButtonListFormInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

   describe('isRadioButtonChecked', () => {
    it('should return false when buttonValue is undefined', () => {
      // GIVEN
      const buttonValue = undefined;

      // WHEN
      const result = component.isRadioButtonChecked(buttonValue);

      expect(result).toBeFalsy();
    });
     it('should return false when buttonValue is null', () => {
       // GIVEN
       const buttonValue = null;

       // WHEN
       const result = component.isRadioButtonChecked(buttonValue);

       expect(result).toBeFalsy();
     });
     it('should return false when buttonValue is not equal to component value', () => {
       // GIVEN
       const value = 'some value';
       const buttonValue = 'some button value';
       component.value = value;
       // WHEN
       const result = component.isRadioButtonChecked(buttonValue);

       expect(result).toBeFalsy();
     });
     it('should return true when buttonValue is equal to component value', () => {
       // GIVEN
       const value = 'some value';
       const buttonValue = value;
       component.value = value;
       // WHEN
       const result = component.isRadioButtonChecked(buttonValue);

       expect(result).toBeTruthy();
     });
  });
});
