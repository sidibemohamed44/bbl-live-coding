import { TextFormInputComponent } from '../reusable-components/text-form-input/text-form-input.component';
import { getReferentielProperty } from '../helpers';
import { SelectBoxInputComponent } from '../reusable-components/select-box-input/select-box-input.component';
import { RadioButtonListFormInputComponent } from '../reusable-components/radio-button-list-form-input/radio-button-list-form-input.component';

export default [
    {
        componentType: TextFormInputComponent,
        data: {
          label: (expertiseData) => getReferentielProperty(expertiseData, 'nom', 'label'),
        },
        customClass: '',
        id: 'nom',
    },
    {
      componentType: TextFormInputComponent,
      data: {
        label: (expertiseData) => getReferentielProperty(expertiseData, 'seb', 'label'),
      },
      customClass: '',
      id: 'seb',
    },
    {
      componentType: RadioButtonListFormInputComponent,
      data: {
        label: (expertiseData) => getReferentielProperty(expertiseData, 'isDancer', 'label'),
        buttonList: (expertiseData) => getReferentielProperty(expertiseData, 'isDancer', 'itemsList'),
      },
      customClass: '',
      id: 'isDancer',
    },
];
