import { Component, Input, OnChanges } from '@angular/core';
import { FormBaseHelperComponent } from '../form-base.component';

@Component({
  selector: 'app-select-box',
  templateUrl: './select-box-input.component.html',
  styleUrls: ['./select-box-input.component.scss']
})
export class SelectBoxInputComponent extends FormBaseHelperComponent {

  @Input() itemsList;
  @Input() side: string;
}
