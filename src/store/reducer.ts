import produce from 'immer';
import * as _ from 'lodash';
import {type, IAppState} from './actions';

export const initialState: IAppState = {
    referentiel: {},
    values: {}
};

export default (state = initialState, action) =>
    produce(state, (draft) => {
        switch (action.type) {
            case type.REFERENTIEL_LOADED:
                draft.referentiel = action.referentiel;
            case type.MISE_A_JOUR_CHAMP:
                draft.values = {
                    ...draft.values,
                    [action.fieldId]: action.value
                };
            break;
        }
    });

