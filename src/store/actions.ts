export interface IAppState {
    referentiel: {};
    values: {
        [key: string]: any,
    };
}

export enum type {
    MISE_A_JOUR_CHAMP = 'mise_a_jour_champ' as any,
    LOAD_REFERENTIEL = 'load_referentiel' as any,
    REFERENTIEL_LOADED = 'referentiel_loaded' as any,
}

export const createActionUpdateFieldValue = (updatedFieldId, value) => ({
  fieldId: updatedFieldId,
  type: type.MISE_A_JOUR_CHAMP.toString(),
  value,
});

export const createActionLoadReferentiel = () => ({
    type: type.LOAD_REFERENTIEL,
});

export const createActionReferentielLoaded = (referentiel) => ({
    referentiel,
    type: type.REFERENTIEL_LOADED,
});