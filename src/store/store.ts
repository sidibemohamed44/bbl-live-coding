import { applyMiddleware, combineReducers, createStore } from 'redux';
import logger from 'redux-logger';
import { composeWithDevTools } from 'redux-devtools-extension';
import reducer from './reducer';
import loadReferentiel from './middleware';

export const store = createStore(combineReducers({data: reducer}), composeWithDevTools(applyMiddleware(loadReferentiel, logger)));
