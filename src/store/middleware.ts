import { type, createActionReferentielLoaded } from './actions';
import axios from 'axios';

export default (store) => (next) => (action) => {
    const out = next(action);
    switch(action.type){
        case type.LOAD_REFERENTIEL:
            axios.get('assets/referentiel.json')
                .then((result) => store.dispatch(createActionReferentielLoaded(result.data)));
    }
    return out;
}