import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgReduxModule, NgRedux } from '@angular-redux/store';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DynamicService } from './services/dynamic.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TextFormInputComponent } from './reusable-components/text-form-input/text-form-input.component';
import { store } from 'src/store/store';
import { IAppState, createActionLoadReferentiel } from 'src/store/actions';
import { SelectBoxInputComponent } from './reusable-components/select-box-input/select-box-input.component';
import { FormsModule } from '@angular/forms';
import { NgSelectModule } from '@ng-select/ng-select';
import {
  RadioButtonListFormInputComponent
} from './reusable-components/radio-button-list-form-input/radio-button-list-form-input.component';

@NgModule({
  declarations: [
    AppComponent,
    TextFormInputComponent,
    SelectBoxInputComponent,
    RadioButtonListFormInputComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgReduxModule,
    NgSelectModule,
    FormsModule
  ],
  providers: [DynamicService],
  bootstrap: [AppComponent],
  entryComponents: [TextFormInputComponent, SelectBoxInputComponent, RadioButtonListFormInputComponent],
})
export class AppModule {
  constructor(ngRedux: NgRedux<{}>) {
    ngRedux.provideStore(store);
    ngRedux.dispatch(createActionLoadReferentiel());
  }
}
